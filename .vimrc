call plug#begin()
Plug 'https://github.com/tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
call plug#end()
set nocompatible
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

let g:airline_symbols.linenr = '  '
let g:airline_symbols.maxlinenr = '  '
let g:airline_symbols.branch = ''
let g:airline_symbols.colnr = ' '
let g:airline#extensions#whitespace#enabled = 0

set number
let g:airline_left_sep=''
let g:airline#extensions#tabline#enabled = 1 " Enable the list of buffers
let g:airline_right_sep=''
set relativenumber
let mapleader = " "

noremap <F5> :set list!<CR>
inoremap <F5> <C-o>:set list!<CR>
cnoremap <F5> <C-c>:set list!<CR>

syntax on
filetype plugin on
nnoremap <leader>t :wincmd v<bar> :Ex <bar> :vertical resize 30<CR>
autocmd FileType python inoremap ;d def<Space>(<++>):<Enter><Tab><++><ESC>kF(i
inoremap <Space><Space> <ESC>/<++><Enter>"_c4l
if has('gui_running')
    set guifont=Droid\ Sans\ Mono\ Slashed\ for\ Powerline
endif

set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<,space:_
